import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TraderTest {

    //Generic setup.
    OrderBook A = new OrderBook();
    Admin Ben= new Admin("Ben", "Borg", 0);
    Trader Wayne= new Trader("Wayne", "Falzon", 1);
    Trader Frank= new Trader("Frank", "Vella", 2);
    Crypto Bitcoin =new Crypto("BTC", 8, 3000);
    Crypto Etheruem =new Crypto("ETH", 8, 5000);
    FIAT Euro = new FIAT("EUR");

    void setupDeposit(){
        Ben.toggleTraderApproval(Wayne);
        Ben.toggleTraderApproval(Frank);

        Wayne.openAccount();
        Frank.openAccount();
    }

    void setupWithdraw(){
        Ben.toggleTraderApproval(Wayne);
        Ben.toggleTraderApproval(Frank);

        Wayne.openAccount();
        Frank.openAccount();

        Wayne.depositToAccount(Bitcoin,10);
    }

    void setupGeneral()
    {
        Ben.establishFIATValue(Euro,1.21);//1 euro = 1.21 dollars
        Ben.establishCryptoValue(Bitcoin, 10);
        Ben.establishCryptoValue(Etheruem, 7.5);

        Ben.toggleTraderApproval(Wayne);
        Ben.toggleTraderApproval(Frank);

        Wayne.openAccount();
        Frank.openAccount();

        Wayne.depositToAccount(Euro, 500.09);
        Frank.depositToAccount(Bitcoin, 20);
    }

    @Test
    void openAccount() {
        //Trader tries to open an accont without being approved.
        Wayne.openAccount();

        assertNull(Wayne.userAccount);

        //Admin approves trader, trader tries to re-open account
        Ben.toggleTraderApproval(Wayne);
        Wayne.openAccount();

        assertNotNull(Wayne.userAccount);
    }

    @Test
    void depositToAccount() {
        setupDeposit();
        //Depositing a new currency to account.
        Wayne.depositToAccount(Bitcoin,10);

        assertEquals(BigDecimal.valueOf(10).setScale(Bitcoin.noDecimal, RoundingMode.HALF_UP), Wayne.userAccount.Balance.get(Bitcoin));

        //Depositing more of already established currency to account
        Wayne.depositToAccount(Bitcoin,10);

        assertEquals(BigDecimal.valueOf(20).setScale(Bitcoin.noDecimal, RoundingMode.HALF_UP), Wayne.userAccount.Balance.get(Bitcoin));


    }

    @Test
    void testDepositToAccount() {

        setupDeposit();
        //Depositing a new currency to account.
        Wayne.depositToAccount(Bitcoin,BigDecimal.valueOf(10));

        assertEquals(BigDecimal.valueOf(10).setScale(Bitcoin.noDecimal, RoundingMode.HALF_UP), Wayne.userAccount.Balance.get(Bitcoin));

        //Depositing more of already established currency to account
        Wayne.depositToAccount(Bitcoin,BigDecimal.valueOf(10));
        assertEquals(BigDecimal.valueOf(20).setScale(Bitcoin.noDecimal, RoundingMode.HALF_UP), Wayne.userAccount.Balance.get(Bitcoin));

    }

    @Test
    void withdrawFromAccount() {
        setupWithdraw();

        Map currBalance = Wayne.userAccount.Balance;
        //Trader tries to withdraw a currency thats non existent in balance
        Wayne.withdrawFromAccount(Euro,5);

        assertEquals(currBalance,Wayne.userAccount.Balance );

        //Trader tries to withdraw more than whats available in balance.
        Wayne.withdrawFromAccount(Bitcoin, 250);

        assertEquals(currBalance,Wayne.userAccount.Balance );

        //Trader tries to withdraw an existing currency in reasonable amount.
        BigDecimal X = Wayne.userAccount.Balance.get(Bitcoin);
        Wayne.withdrawFromAccount(Bitcoin, 5);
        assertEquals(X.subtract(BigDecimal.valueOf(5)).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP),Wayne.userAccount.Balance.get(Bitcoin) );
    }

    @Test
    void testWithdrawFromAccount() {

        setupWithdraw();

        Map currBalance = Wayne.userAccount.Balance;
        //Trader tries to withdraw a currency thats non existent in balance
        Wayne.withdrawFromAccount(Euro,BigDecimal.valueOf(5));

        assertEquals(currBalance,Wayne.userAccount.Balance );

        //Trader tries to withdraw more than whats available in balance.
        Wayne.withdrawFromAccount(Bitcoin, BigDecimal.valueOf(250));

        assertEquals(currBalance,Wayne.userAccount.Balance );

        //Trader tries to withdraw an existing currency in reasonable amount.
        BigDecimal X = Wayne.userAccount.Balance.get(Bitcoin);
        Wayne.withdrawFromAccount(Bitcoin, BigDecimal.valueOf(5));
        assertEquals(X.subtract(BigDecimal.valueOf(5)).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP),Wayne.userAccount.Balance.get(Bitcoin) );
    }

    @Test
    void placeMarketBuyOrder() {
        setupGeneral();

        //Buyer places order he can't afford.
        Wayne.placeMarketBuyOrder(Bitcoin,1000,A,Euro);
        assertEquals(0, A.currBuyOrders.size());

        //TODO: Check that within circulation

        //Buyer places order he can afford
        Wayne.placeMarketBuyOrder(Bitcoin,10,A,Euro);

        assertEquals(1, A.currBuyOrders.size());
    }

    @Test
    void placeMarketSellOrder() {
        setupGeneral();

        //Seller places order for currency he doesn't own.
        Frank.placeMarketSellOrder(Etheruem,6.05,A,Euro);
        assertEquals(0, A.currSellOrders.size());

        //Seller places order for currency he owns but in amounts he doesn't own.
        Frank.placeMarketSellOrder(Bitcoin,1000,A,Euro);
        assertEquals(0, A.currSellOrders.size());

        //Seller places valid order.
        Frank.placeMarketSellOrder(Bitcoin,6.05,A,Euro);
        assertEquals(1, A.currSellOrders.size());
    }

    @Test
    void placeLimitBuyOrder() {
        setupGeneral();

        //Buyer places order he can't afford.
        Wayne.placeLimitBuyOrder(Bitcoin,1000,5,A,Euro);
        assertEquals(0, A.currBuyOrders.size());

        //TODO: Check that within circulation

        //Buyer places order he can afford
        Wayne.placeLimitBuyOrder(Bitcoin,10,5,A,Euro);

        assertEquals(1, A.currBuyOrders.size());
    }

    @Test
    void placeLimitSellOrder() {
        setupGeneral();

        //Seller places order for currency he doesn't own.
        Frank.placeLimitSellOrder(Etheruem,6.05,20,A,Euro);
        assertEquals(0, A.currSellOrders.size());

        //Seller places order for currency he owns but in amounts he doesn't own.
        Frank.placeLimitSellOrder(Bitcoin,1000,20,A,Euro);
        assertEquals(0, A.currSellOrders.size());

        //Seller places valid order.
        Frank.placeLimitSellOrder(Bitcoin,6.05,20,A,Euro);
        assertEquals(1, A.currSellOrders.size());
    }

    @Test
    void orderMatching1()
    {
        //Case for perfect match of two orders.
        setupGeneral();
        double amt0 = 2.75;


        Wayne.placeMarketBuyOrder(Bitcoin,amt0,A,Euro);
        Frank.placeMarketSellOrder(Bitcoin,amt0,A,Euro);

        //Checks for correctness of exchange
        assertEquals(BigDecimal.valueOf(amt0).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP),Wayne.userAccount.Balance.get(Bitcoin));
        assertEquals(Euro.DollarToCurr(Bitcoin.CurrToDollar(BigDecimal.valueOf(amt0).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP))),Frank.userAccount.Balance.get(Euro));

        //Checks to see that fully filled orders are discarded.
        assertEquals(0, A.currBuyOrders.size());
        assertEquals(0, A.currSellOrders.size());
    }

    @Test
    void orderMatching2()
    {
        //Case for partial match of two orders.
        setupGeneral();
        double amt0 = 2.75;
        double amt1 = 1.25;


        Wayne.placeMarketBuyOrder(Bitcoin,amt0,A,Euro);
        Frank.placeMarketSellOrder(Bitcoin,amt1,A,Euro);

        //Checks for correctness of exchange
        assertEquals(BigDecimal.valueOf(amt1).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP),Wayne.userAccount.Balance.get(Bitcoin));
        assertEquals(Euro.DollarToCurr(Bitcoin.CurrToDollar(BigDecimal.valueOf(amt1).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP))),Frank.userAccount.Balance.get(Euro));

        /*
           Checks to see that fully filled orders are discarded and partially filled orders
           have had their volume executed value correctly updated.
         */
        assertEquals(1, A.currBuyOrders.size());
        assertEquals(BigDecimal.valueOf(amt1).setScale(Bitcoin.noDecimal,RoundingMode.HALF_UP),A.currBuyOrders.get(0).volumeExecuted);
        assertEquals(0, A.currSellOrders.size());
    }

    @Test
    void orderMatching3()
    {
        //Case for Limit orders.
        setupGeneral();
        double amt0 = 2.75;


        Wayne.placeLimitBuyOrder(Bitcoin,amt0,7.5,A,Euro);
        Frank.placeLimitSellOrder(Bitcoin,amt0,7,A,Euro);

        //Price of bitcoin is at 10, the limit buy order can't be executed. Hence neither order should execute.
        assertEquals(1, A.currSellOrders.size());
        assertEquals(1, A.currBuyOrders.size());

        //The price of Bitcoin fluctuates and hence the limit orders can execute.
        Ben.updateCurrValue(Bitcoin,9,A);

        assertEquals(0, A.currSellOrders.size());
        assertEquals(0, A.currBuyOrders.size());
    }


    @Test
    void cancelUnfilledOrders()
    {
        setupGeneral();
        double amt0 = 2.75;
        Wayne.depositToAccount(Etheruem, 50);
        Wayne.placeLimitBuyOrder(Bitcoin,amt0,7.5,A,Euro);
        Wayne.placeLimitBuyOrder(Bitcoin,amt0,7.5,A,Euro);
        Frank.placeLimitSellOrder(Bitcoin,amt0,100,A,Euro);
        Wayne.placeMarketSellOrder(Etheruem,20,A,Euro);

        assertEquals(2, A.currBuyOrders.size());
        assertEquals(2, A.currSellOrders.size());

        Wayne.cancelUnfilledOrders(A);


        //After Wayne cancels his unfilled orders, there should only be no buy orders left, and only a single sell order.
        assertEquals(0, A.currBuyOrders.size());
        assertEquals(1, A.currSellOrders.size());


    }
}