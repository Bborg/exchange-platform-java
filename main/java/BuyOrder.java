import java.math.BigDecimal;
import java.math.RoundingMode;

public class BuyOrder {
    protected Trader trader;//Trader making buy order.
    protected Crypto curr;//Currency to be bought
    protected FIAT fiat;//fiat to buy currency with.
    protected BigDecimal quantity;//Quantity to be bought.
    protected BigDecimal volumeExecuted;//Amount alreayd executed.

    //Constructor for buy order.
    public BuyOrder(Trader trader, Crypto curr, BigDecimal quantity, FIAT fiat){
        this.trader =trader;
        this.curr = curr;
        this.fiat = fiat;
        this.quantity = quantity;
        this.volumeExecuted = new BigDecimal(0).setScale(curr.noDecimal, RoundingMode.HALF_UP);
    }
}
