import java.math.BigDecimal;

public class LimitBuyOrder extends BuyOrder{
    protected BigDecimal bidPrice;

    public LimitBuyOrder(Trader trader, Crypto curr, BigDecimal quantity, BigDecimal bidPrice, FIAT fiat)
    {
        super(trader, curr, quantity,fiat);
        this.bidPrice=bidPrice;
    }
}
