import java.math.BigDecimal;
import java.math.RoundingMode;

public class Admin extends User {

    public Admin(String name, String surname, int ID){ //Admin constructor
        super(name,surname,ID); //Since admin has the same attributes as its super class we can recycle the same constructor.
    }

    //Toggles the boolean value Approval within trader.
    public void toggleTraderApproval(Trader trader){
        trader.Approval =!trader.Approval;
        System.out.println("Approval status of trader, " + trader.ID + " set to "+trader.Approval+"\n");
    }


    //Assigns a value in dollar, of a crypto currency.
    public void establishCryptoValue(Crypto crypto, double dollarValue)
    {
        crypto.valueDollar = new BigDecimal(dollarValue).setScale(2, RoundingMode.HALF_UP);
    }

    //Assigns a value in dollar, of a FIAT currency.
    public void establishFIATValue(FIAT curr, double dollarValue)
    {
        curr.valueDollar = new BigDecimal(dollarValue).setScale(2, RoundingMode.HALF_UP) ;
    }

    //Updates Currencys Dollar Value.
    public void updateCurrValue(Currency curr,double updatedValue, OrderBook orderBook)
    {
        curr.valueDollar = new BigDecimal(updatedValue).setScale(2,RoundingMode.HALF_UP);

        //Checks if any limit orders can be executed post fluctuation in currency price.
        orderBook.execOrders();
    }
}
