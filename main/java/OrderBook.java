import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OrderBook {
    //Lists of open Buy Orders and Sell orders.
    protected List<BuyOrder> currBuyOrders;
    protected List<SellOrder> currSellOrders;

    //Constructor initialises empty lists.
    public OrderBook()
    {
        this.currBuyOrders= new ArrayList<>();
        this.currSellOrders= new ArrayList<>();
    }

    public void addBuyOrder(BuyOrder newOrder)
    {
        currBuyOrders.add(newOrder);
        execOrders();
    }

    public void addSellOrder(SellOrder newOrder)
    {
        currSellOrders.add(newOrder);
        execOrders();
    }

    //Removes filled orders.
    private void cleanOrders()
    {
        currBuyOrders.removeIf(B -> Objects.equals(B.volumeExecuted, B.quantity));
        currSellOrders.removeIf(S -> Objects.equals(S.volumeExecuted, S.quantity));

    }

    //Looks for orders which are able to be executed.
    public void execOrders()
    {
        //If no orders exists
        if(currBuyOrders == null || currSellOrders == null){
            return;
        }


        for (BuyOrder B : currBuyOrders) {

            //Check if limit buy order can execute.
            if((B.getClass() == LimitBuyOrder.class))
            {
               BigDecimal dollarBid =B.fiat.CurrToDollar(((LimitBuyOrder) B).bidPrice);
                if(dollarBid.compareTo(B.curr.valueDollar)<=0) {
                    System.out.println("Limit Buy order cannot be executed for now.\n");
                    continue;
                }
            }
            for (SellOrder S : currSellOrders) {

                //Check if limit sell order can execute.
                if((S.getClass() == LimitSellOrder.class))
                {
                    BigDecimal dollarAsk =S.fiat.CurrToDollar(((LimitSellOrder) S).askPrice);
                    if(dollarAsk.compareTo(S.curr.valueDollar)==1) {
                        System.out.println("Limit Sell order cannot be executed for now.\n");
                        continue;
                    }
                }
                //If Buy and Sell orders match by Currency and Amount we can fill the order.
                if (Objects.equals(B.curr.symbol, S.curr.symbol) && (B.volumeExecuted.compareTo(B.quantity) <0 ) &&(S.volumeExecuted.compareTo(S.quantity)<0)) {
                    fillOrders(B,S);
                }
            }
        }
        //filled orders are removed.
        cleanOrders();
    }

    //Fills/Partially fills orders as they are passed by `execOrders`
    private void fillOrders(BuyOrder B, SellOrder S)
    {

        if ((B.quantity.subtract(B.volumeExecuted)).compareTo((S.quantity.subtract(S.volumeExecuted))) >=0)
        {
            BigDecimal execVol = (S.quantity.subtract(S.volumeExecuted));

            S.trader.withdrawFromAccount(S.curr,execVol);//Update seller balance
            S.trader.depositToAccount(S.fiat,S.fiat.DollarToCurr(S.curr.CurrToDollar(execVol)));
            S.volumeExecuted=S.quantity;//Fill sell order

            B.trader.depositToAccount(B.curr,execVol);//Updated buyer balance
            B.volumeExecuted = B.volumeExecuted.add(execVol);//Partially fill buy order


        }else{

            BigDecimal execVol = (B.quantity.subtract(B.volumeExecuted));
            BigDecimal fiatExchange =B.curr.CurrToDollar(execVol);

            //Update buyer balance
            B.trader.withdrawFromAccount(B.fiat,B.fiat.DollarToCurr(fiatExchange));
            B.trader.depositToAccount(B.curr,execVol);

            //Fill buy order
            B.volumeExecuted=B.quantity;

            //Update seller balance
            S.trader.withdrawFromAccount(B.curr,execVol);
            S.trader.depositToAccount(S.fiat,S.fiat.DollarToCurr(fiatExchange));

            //Partially fill sell order
            S.volumeExecuted = S.volumeExecuted.add(execVol);

        }
    }


    public void cancelOrderByTrader(Trader trader)//Called From Trader
    {
        for (BuyOrder B : currBuyOrders)
        {
            if(B.trader == trader)
            {
                B.volumeExecuted=B.quantity;
            }
        }
        for (SellOrder S : currSellOrders)
        {
            if(S.trader == trader)
            {
                S.volumeExecuted=S.quantity;
            }
        }
        cleanOrders();

        System.out.println("All unfilled orders for trader "+ trader.ID+" cancelled.\n");
    }

}
