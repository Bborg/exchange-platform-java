import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//TODO: Write Bash Scripts
//TODO: Check that order not larger than circulation
public class Trader extends User {
    boolean Approval; //Approval true lets user open an account and make use of the system.
    Account userAccount; //Account holds users Balance.

    //Trader constructor
    public Trader(String name, String surname, int ID){
        super(name,surname,ID); //Since trader has the same attributes as its super class we can recycle the same constructor.
        this.Approval = false; //Approval status automatically set to false.
        this.userAccount =null;//initially no user account exists.
    }

    //Account is a proctected class within user.
    protected class Account {

        //Contains user balance
        Map<Currency, BigDecimal> Balance;

        //Constructor for account.
        public Account ()
        {
            //Creates empty hashmap for balance.
            this.Balance= new HashMap<>();

        }
    }

    //Function to open account for user.
    public void openAccount() {

        //Checks to see if user has permission to open account.
        if(!Approval){
            System.out.println("User needs admin approval to open an account!\n");
            return;
        }

        //One account per user, if user already has an account another cannot be created.
        if(this.userAccount != null){
            System.out.println("User already has an open account!\n");
            return;
        }

        //If all checks are met a new account is created.
        this.userAccount= new Account();
        System.out.println("Account successfully created, user can now start using the exchange!\n");
    }

    //Function to deposit to account.
    public void depositToAccount(Currency curr, BigDecimal Amount){

        //Checks to see whether user has an account.
        if(this.userAccount == null){
            System.out.println("User must open an account to be able to deposit!\n");
            return;
        }

        //Checks to see whether user already has funds of that currency within his account.
        BigDecimal currAmnt = this.userAccount.Balance.get(curr);


        /*
          If no key for the currency exists in the map, we create it and assign the deposited amount.
          Else we update the value by adding the current amount with the amount being deposited.
        */
        if( currAmnt== null){
            this.userAccount.Balance .put(curr, Amount.setScale(curr.noDecimal, RoundingMode.HALF_UP));
        }else{
            this.userAccount.Balance.put(curr, Amount.add(currAmnt).setScale(curr.noDecimal,RoundingMode.HALF_UP));
        }
        System.out.println("Amount of " + Amount + " in " + curr.symbol +" deposited into account of user, " +this.ID+"\n");
    }

    public void depositToAccount(Currency curr, double amount1){
        BigDecimal Amount = new BigDecimal(amount1).setScale(curr.noDecimal,RoundingMode.HALF_UP);
        if(this.userAccount == null){
            System.out.println("User must open an account to be able to deposit!\n");
            return;
        }

        BigDecimal currAmnt = this.userAccount.Balance.get(curr);
        if( currAmnt== null){
            this.userAccount.Balance .put(curr, Amount.setScale(curr.noDecimal, RoundingMode.HALF_UP));
        }else{
            this.userAccount.Balance.put(curr, Amount.add(currAmnt).setScale(curr.noDecimal,RoundingMode.HALF_UP));
        }
        System.out.println("Amount of " + Amount + " in " + curr.symbol +" deposited into account of user, " +this.ID+"\n");
    }

    //Function to withdraw funds from account.
    public void withdrawFromAccount(Currency curr, BigDecimal Amount){

        if(this.userAccount == null){
            System.out.println("User must open an account to be able to withdraw!\n");
            return;
        }

        BigDecimal currAmt;
        try{
            currAmt  = this.userAccount.Balance.get(curr);
            if(currAmt == null)
            {
                throw(null);
            }
        }catch(NullPointerException e)
        {
            System.out.println("Error: Currency to withdraw does not exist in balance!!\n");
            return;
        }

        if(currAmt.compareTo(Amount) >=0)
        {
            this.userAccount.Balance .put(curr, currAmt.subtract(Amount));
            System.out.println("Amount of " + Amount + " in " + curr.symbol +" withdrawn from account of user, " +this.ID+"\n");
            return;
        }
        System.out.println("Insufficient Balance!\n");
        return;
    }

    public void withdrawFromAccount(Currency curr, double amount1){
        BigDecimal Amount = new BigDecimal(amount1).setScale(curr.noDecimal, RoundingMode.HALF_UP);

        if(this.userAccount == null){
            System.out.println("User must open an account to be able to withdraw!\n");
            return;
        }

        BigDecimal currAmt;
        try{
           currAmt  = this.userAccount.Balance.get(curr);
           if(currAmt == null)
           {
               throw(null);
           }
        }catch(NullPointerException e)
        {
            System.out.println("Error: Currency to withdraw does not exist in balance!!\n");
            return;
        }

        if(currAmt.compareTo(Amount) >=0)
        {
            this.userAccount.Balance .put(curr, currAmt.subtract(Amount));
            System.out.println("Amount of " + Amount + " in " + curr.symbol +" withdrawn from account of user, " +this.ID+"\n");
            return;
        }
        System.out.println("Insufficient Balance!\n");
        return;
    }


    //Functions to place Market orders
    public void placeMarketBuyOrder(Crypto curr, double amount1, OrderBook book, FIAT fiat) {
        BigDecimal amount = new BigDecimal(amount1).setScale(curr.noDecimal,RoundingMode.HALF_UP);

        if(this.userAccount == null){
            System.out.println("User must open an account to be able to place an order!\n");
            return;
        }

        try{
            BigDecimal currAmt = this.userAccount.Balance.get(fiat);
            if(currAmt == null)
            {
                throw(null);
            }
        }catch(NullPointerException e)
        {
            System.out.println("Error: Currency used to buy does not exist in balance!!\n");
            return;
        }

        if(curr.CurrToDollar(amount) .compareTo(fiat.CurrToDollar(userAccount.Balance.get(fiat)))>=0)
        {
            System.out.println("Insufficient Balance to make buy order!\n");
            return;
        }

        MarketBuyOrder newOrder = new MarketBuyOrder(this,curr,amount, fiat);
        book.addBuyOrder(newOrder);
        System.out.println("Market Buy Order, for " + curr.symbol+ " in amount of " +amount+" placed.\n");
    }

    public void placeMarketSellOrder(Crypto curr, double amount1, OrderBook book,FIAT fiat) {
            BigDecimal amount = new BigDecimal(amount1).setScale(curr.noDecimal,RoundingMode.HALF_UP);

            try{
                BigDecimal currAmt = this.userAccount.Balance.get(curr);
                if(currAmt == null)
                {
                    throw(null);
                }
            }catch(NullPointerException e)
            {
                System.out.println("Error: Currency used to sell does not exist in balance!!\n");
                return;
            }

            if(this.userAccount == null){
                System.out.println("User must open an account to be able to place an order!\n");
                return;
            }

            if(amount.compareTo(userAccount.Balance.get(curr)) ==1)
            {
                System.out.println("Insufficient Balance to make buy order!\n");
                return;
            }

            MarketSellOrder newOrder = new MarketSellOrder(this,curr,amount,fiat);
            book.addSellOrder(newOrder);
            System.out.println("Market Sell Order, for " + curr.symbol+ " in amount of " +amount+" placed.\n");
    }

    //Functions to place limit orders
    public void placeLimitBuyOrder(Crypto curr, double amount1, double bidPrice1, OrderBook book,FIAT fiat) {
        BigDecimal amount = new BigDecimal(amount1).setScale(curr.noDecimal,RoundingMode.HALF_UP);
        BigDecimal bidPrice = new BigDecimal(bidPrice1).setScale(2, RoundingMode.HALF_UP);

        if(this.userAccount == null){
            System.out.println("User must open an account to be able to place an order!\n");
            return;
        }

        try{
            BigDecimal currAmt = this.userAccount.Balance.get(fiat);
            if(currAmt == null)
            {
                throw(null);
            }
        }catch(NullPointerException e)
        {
            System.out.println("Error: Currency to sell does not exist in balance!!\n");
            return;
        }

        if((amount .multiply(fiat.CurrToDollar(bidPrice))). compareTo(fiat.CurrToDollar(userAccount.Balance.get(fiat))) ==1)
        {
            System.out.println("Insufficient Balance to make buy order!\n");
            return;
        }

        LimitBuyOrder newOrder = new LimitBuyOrder(this,curr,amount,bidPrice,fiat);
        book.addBuyOrder(newOrder);
        System.out.println("Limit Buy Order, for " + curr.symbol+ " in amount of " +amount+" at price "+ bidPrice+ " placed.\n");
    }

    public void placeLimitSellOrder(Crypto curr, double amount1, double askPrice1, OrderBook book, FIAT fiat) {
        BigDecimal amount = new BigDecimal(amount1).setScale(curr.noDecimal,RoundingMode.HALF_UP);
        BigDecimal askPrice = new BigDecimal(askPrice1).setScale(2, RoundingMode.HALF_UP);


        if(this.userAccount == null){
            System.out.println("User must open an account to be able to place an order!\n");
            return;
        }

        try{
            BigDecimal currAmt = this.userAccount.Balance.get(curr);
            if(currAmt == null)
            {
                throw(null);
            }
        }catch(NullPointerException e)
        {
            System.out.println("Error: Currency to sell does not exist in balance!!\n");
            return;
        }

        if(amount.compareTo(userAccount.Balance.get(curr)) ==1)
        {
            System.out.println("Insufficient Balance to make buy order!\n");
            return;
        }

        LimitSellOrder newOrder = new LimitSellOrder(this,curr,amount,askPrice, fiat);
        book.addSellOrder(newOrder);
        System.out.println("Limit Buy Order, for " + curr.symbol+ " in amount of " +amount+" at price "+ askPrice+ " placed.\n");
    }


    public void cancelUnfilledOrders(OrderBook A)
    {
        A.cancelOrderByTrader(this);
    }

}
