import java.math.BigDecimal;

public class MarketBuyOrder extends BuyOrder{

    public MarketBuyOrder(Trader trader, Crypto curr, BigDecimal quantity, FIAT fiat)
    {
        super(trader, curr, quantity, fiat);
    }
}
