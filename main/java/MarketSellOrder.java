import java.math.BigDecimal;

public class MarketSellOrder extends SellOrder{

    public MarketSellOrder(Trader trader, Crypto curr, BigDecimal quantity, FIAT fiat)
    {
        super(trader, curr, quantity,fiat);
    }
}
