public class FIAT extends Currency {

    public FIAT (String symbol){
        super(symbol, 2); //All fiat currency have a decimal value of 2.
    }
}
