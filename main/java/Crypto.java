public class Crypto extends Currency {
    int totalSupply;//total supply of the crypto currency currently in circulation.

    //Constructor.
    public Crypto (String symbol, int noDecimal, int totalSupply)
    {
        super(symbol, noDecimal);//calls Currency constructor
        this.totalSupply = totalSupply;
    }
}
