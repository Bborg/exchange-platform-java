import java.math.BigDecimal;
import java.math.RoundingMode;

public class Currency {
    public final String symbol;
    int  noDecimal;//number of decimal places after the point.
    BigDecimal valueDollar;//Value in dollar used as base reference for conversions.

    //Constructor for currency.
    public Currency (String symbol, int noDecimal){
        this.symbol = symbol;
        this.noDecimal = noDecimal;
        this.valueDollar= new BigDecimal("0").setScale(2,RoundingMode.HALF_UP);

    }


    //Returns dollar value of the amount of currency passed as a parameter.
    public BigDecimal CurrToDollar(BigDecimal currAmount)
    {
        return (valueDollar.multiply(currAmount)).setScale(2, RoundingMode.HALF_UP);
    }

    //Returns amount of currency given dollar value passed as a parameter.is
    public  BigDecimal DollarToCurr(BigDecimal valueDollarAmount)
    {
        return((valueDollarAmount.divide(valueDollar,noDecimal,RoundingMode.HALF_UP)));
    }
}
