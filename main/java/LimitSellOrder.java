import java.math.BigDecimal;

public class LimitSellOrder extends SellOrder {

   protected BigDecimal askPrice;

    public LimitSellOrder(Trader trader, Crypto curr, BigDecimal quantity, BigDecimal askPrice, FIAT fiat)
    {
        super(trader, curr, quantity, fiat);
        this.askPrice=askPrice;
    }
}
