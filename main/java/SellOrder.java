import java.math.BigDecimal;
import java.math.RoundingMode;

public class SellOrder {
    Trader trader;//Trader who makes the order
    Crypto curr;//Crypto to be sold
    FIAT fiat;//What Fiat currency the trader wants in return.
    BigDecimal quantity;//Amount to be sold.
    BigDecimal volumeExecuted;//volume already executed.

    //Constructor for sell order.
    public SellOrder(Trader trader, Crypto curr, BigDecimal quantity,FIAT fiat){
        this.trader =trader;
        this.curr = curr;
        this.quantity = quantity;
        this.volumeExecuted = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
        this.fiat = fiat;
    }
}
