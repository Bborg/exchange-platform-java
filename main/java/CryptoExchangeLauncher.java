
public class CryptoExchangeLauncher
{
    public static void main (String [] args)
    {
        OrderBook A = new OrderBook();
        Admin Ben= new Admin("Ben", "Borg", 0);
        Trader Wayne= new Trader("Wayne", "Falzon", 1);
        Trader Frank= new Trader("Frank", "Vella", 2);
        Crypto Bitcoin =new Crypto("BTC", 8, 3000);
        Crypto Etheruem =new Crypto("ETH", 8, 5000);
        FIAT Euro = new FIAT("EUR");


        Ben.establishFIATValue(Euro,1.21);//1 euro = 1.21 dollars
        Ben.establishCryptoValue(Bitcoin, 10);
        Ben.establishCryptoValue(Etheruem, 7.5);

        Ben.toggleTraderApproval(Wayne);
        Ben.toggleTraderApproval(Frank);

        Wayne.openAccount();
        Frank.openAccount();

        Wayne.depositToAccount(Euro, 500.09);
        Wayne.depositToAccount(Euro, 5);
        Frank.depositToAccount(Bitcoin, 20.2);
        Frank.withdrawFromAccount(Etheruem, 2);

        Wayne.placeMarketBuyOrder(Bitcoin, 2.4, A, Euro);
        Frank.placeMarketSellOrder(Bitcoin, 20.2,A,Euro);
        Wayne.placeLimitBuyOrder(Bitcoin,30.0,7.0,A,Euro);

        System.out.println("Finished");
    }
}
