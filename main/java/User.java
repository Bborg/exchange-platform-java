public class User {
    public String name;
    public String surname;
    public final int ID; //Way of identifying the user
    //Final as this should not be changed, also must be unique.



    /*name = the name of the user
      surname = the surname of the user
      id = a unique number to be able to identify each user
    */
    public User(String name, String surname, int id){ //User constructor
        //Setting data for user based on values passed as parameters.
        this.name = name;
        this.surname = surname;
        this.ID = id;
    }


}
